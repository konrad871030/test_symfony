<?php
return [
  'Please sign in' => 'Zaloguj się do serwisu',
    'Email'=>'Email',
    'Password'=>'Hasło',
    'Sign in'=>'Zaloguj',
    'Sign OUT'=>'Wyloguj się',
    'Sign UP'=>'Rejestracja',
    'Sign IN'=>'Logowanie',
    'Continue reading'=>'Czytaj dalej...'
];

