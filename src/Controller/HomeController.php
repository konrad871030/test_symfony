<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;

class HomeController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
    }
    /**
     * @Route("/home", name="home")
     */
    public function index(RegistryInterface $registry)
    {
        $user = $this->security->getUser();
        $postEntity = new \App\Repository\PostsRepository($registry);
        $posts=$postEntity->findAll();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'user'=>$user,
            'posts'=>$posts
        ]);
    }
}
